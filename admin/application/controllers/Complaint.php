<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('complaint_model','cm');
	}

	function index(){
		$data['jsapp'] = array('admin/complaint');
		$this->load->view('header',$data);
		$this->load->view('complaint');
		$this->load->view('footer');
	}

	function get(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->cm->CountDataComplaint();
		
        $row = array();
		$results = $this->cm->getDataComplaint($length,$start, $def['order'], 'asc');
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"					=> $ii,
					'nik_pelapor'			=> $d->nik_pengadu,
					'nama_pelapor'			=> $d->nama_pengadu,
					'alamat_pelapor'		=> $d->alamat_pengadu,
					'desa_pelapor'			=> $d->nama_desa_pengadu,
					'kecamatan_pelapor'		=> $d->nama_kec_pengadu,
					'nik_terlapor'			=> $d->nik,
					'nama_terlapor'			=> $d->nama,
					'alamat_terlapor'		=> $d->alamat,
					'nama_desa'				=> $d->nama_desa,
					'nama_kec'				=> $d->nama_kec,
					'nama_bantuan'				=> $d->nama_bantuan,
					'keterangan_pelaporan'				=> $d->id_jenis_pengaduan,
					
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);

	} 

}

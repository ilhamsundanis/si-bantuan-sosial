<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eadmin extends CI_Controller {

	public function index()
	{
		//$data['jsapp'] = array('admin/main_dashboard');
		/* $this->load->view('header',$data);
		$this->load->view('main');
		$this->load->view('footer'); */
		
		$this->load->view('HeaderEadmin');
		$this->load->view('MainEadmin');
		$this->load->view('footerEadmin');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('dashboard_model','dshm');
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/main_dashboard');
		// all count data is
		$data['CProvAll'] = $this->dshm->countAllBansos(2);
		$data['CPresidenAll'] = $this->dshm->countAllBansos(1);
		$data['CBupatiAll'] = $this->dshm->countAllBansos(3);
		$data['CDdAll'] = $this->dshm->countAllBansos(4);
		$data['CSembakoRegulerAll'] = $this->dshm->countAllBansos(5);
		$data['CSembakoPerluasanAll'] = $this->dshm->countAllBansos(6);
		$data['CBltAll'] = $this->dshm->countAllBansos(7);
		$data['CPkhAll'] = $this->dshm->countAllBansos(8);

		$data['CDtksProv'] = $this->dshm->countDtksBansos(2);
		$data['CNonDtksProv'] = $this->dshm->countNonDtksBansos(2);

		$data['CDtksPresiden'] = $this->dshm->countDtksBansos(1);
		$data['CNonDtksPresiden'] = $this->dshm->countNonDtksBansos(1);

		$data['CDtksBupati'] = $this->dshm->countDtksBansos(3);
		$data['CNonDtksBupati'] = $this->dshm->countNonDtksBansos(3);

		$data['CDtksDd'] = $this->dshm->countDtksBansos(4);
		$data['CNonDtksDd'] = $this->dshm->countNonDtksBansos(4);

		$data['CDtksSembakoReguler'] = $this->dshm->countDtksBansos(5);
		$data['CNonDtksSembakoReguler'] = $this->dshm->countNonDtksBansos(5);

		$data['CDtksSembakoPerluasan'] = $this->dshm->countDtksBansos(6);
		$data['CNonDtksSembakoPerluasan'] = $this->dshm->countNonDtksBansos(6);

		$data['CDtksBlt'] = $this->dshm->countDtksBansos(7);
		$data['CNonDtksBlt'] = $this->dshm->countNonDtksBansos(7);
		
		$data['CDtksPkh'] = $this->dshm->countDtksBansos(8);
		$data['CNonDtksPkh'] = $this->dshm->countNonDtksBansos(8);

		$this->load->view('header',$data);
		$this->load->view('main');
		$this->load->view('footer');
	}

	public function GraphDtKsNonDtksKec(){

		$dataDashDtksKec = $this->dshm->GraphDtKsNonDtksKec();
		if(count($dataDashDtksKec) != 0){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dataDashDtksKec
			);
			echo json_encode($output);
		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data tidak ada',
				'data'	=> null
			);
			echo json_encode($output);
		}
	}
}

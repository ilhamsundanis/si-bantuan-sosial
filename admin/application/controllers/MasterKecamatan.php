<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MasterKecamatan extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('Wilayah_model','wm');
	}

	public function index()
	{
		
		$data['jsapp'] = array('admin/master_kecamatan');
		

		$this->load->view('header',$data);
		$this->load->view('master_kecamatan');
		$this->load->view('footer');
	}

	function get(){
		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		$namaKecamatan = $this->input->post('nama_kecamatan');
		

		$result = array();
        $recordsTotal = $this->wm->count_data_kecamatan($namaKecamatan);
		
        $row = array();
		$results = $this->wm->getDataKecamatans($length,$start, $def['order'], 'asc',$namaKecamatan);
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"					=> $ii,
					'nama'					=> $d->nama_kecamatan,
					"aksi" 	    			=> '<button type="button" class="btn btn-info btn-flat btn-sm" onclick="EditKec('.$d->kode_kec.');"><i class="fa fa-edit"></i> Edit Kecamatan</button>'
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}

	function SaveKecamatan(){
		$kode_kec = $this->input->post('kode_kec');
		$name = $this->input->post('name');
		$id_kecamatan = $this->input->post('id_kecamatan');

		$tableName = 'master.tb_kec';

		if($id_kecamatan == -1){

			$data_insert = array(
				'kode_kec'	=> $kode_kec,
				'kode_kab'	=> 3201, // hardcode kode kabupate
				'name'		=> $name
			);		

			$insertKec = $this->wm->SaveKecamatan($id_kecamatan,$data_insert,$tableName);
			if($insertKec){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil diinput'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Gagal diinput'
				);

				echo json_encode($output);
			}
		}else{
			$data_update = array(
				'name'		=> $name
			);		
			$update = $this->wm->SaveKecamatan($kode_kec,$data_update,$tableName);
			if($update){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Berhasil dirubah'
				);

				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Gagal dirubah'
				);

				echo json_encode($output);
			}
		}

	}

	function GetDataKecByKode(){

		$kodeKec = $this->input->post('kodeKec');
		$dtKecByKode = $this->wm->GetDataKecByKode($kodeKec);
		if(count($dtKecByKode) != 0){
			$dt = array(
				'kode_kec'	=> $dtKecByKode->kode_kec,
				'name'	=> $dtKecByKode->name,
			);
			$output = array(
				'state'	=> false,
				'msg'	=> null,
				'data'	=> $dt
			);

			echo json_encode($output);

		}else{
			$output = array(
				'state'	=> false,
				'msg'	=> 'Data Kecamatan tidak ada',
				'data'	=> null
			);

			echo json_encode($output);
		}

	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';

//lets Use the Spout Namespaces
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class MgtBansos extends CI_Controller {

	function __construct() {
        parent:: __construct();
        $this->load->model('bansos_model','bm');
	}

	function index(){
		
		$data['jsapp'] = array('admin/mgt_bansos');
		$this->load->view('header',$data);
		$this->load->view('mgtbansos');
		$this->load->view('footer');
	}

	function get(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		$kode_kec = $this->input->post('kode_kec');
		$kode_desa = $this->input->post('kode_desa');
		$nik = $this->input->post('nik');
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->bm->count_data($kode_kec, $kode_desa, $nik);
		
        $row = array();
		$results = $this->bm->get_data($length,$start, $def['order'], 'asc',$kode_kec, $kode_desa, $nik);
		
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"				=> $ii,
					"nama" 				=> substr_replace($d->nama, '..XXX', -3, 1) ,
					"nik" 				=> substr_replace($d->nik, '..XXX', -3, -1) ,
					"alamat" 			=> $d->alamat,
					"kecamatan" 		=> $d->nama_kecamatan,
					"desa" 				=> $d->nama_desa,
					"nama_bantuan" 		=> $d->nama_bantuan,
					"status" 			=> ($d->status == true) ? "YA" : "TIDAK",
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
	}




		

	function getDataKpm(){

		$order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
			'draw' => $this->input->post('draw'),
			'length' => $this->input->post('length'),
			'order' => $column[$idx_cols]['name'],
			'start' => $this->input->post('start'),
			'dir' => $order[0]['dir']
		);
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		$nama = $this->input->post('nama');

		$result = array();
		$recordsTotal = $this->bm->count_data_kpm($nama);
		
		$row = array();
		$results = $this->bm->getKpmByKodeWilayah($length,$start, $def['order'], 'asc',$nama);
		$dd = "";

		if (count($results) > 0) {
			$ii = $start;
			
			foreach ($results as $d) {
				$ii++;
				
				$row[] = array
				(
					"no"				=> $ii,
					"id_kpm"				=> $d->id_kpm,
					"nama" 			=> $d->nama,
					"nik" 			=> $d->nik,
					"alamat" 			=> $d->alamat,
				);
			}
		}
	
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);

	}

	function getBantuan(){

		$dataBantuan = $this->bm->getBantuan();

		if(count($dataBantuan) > 0){
			$output = array(
				'state'	=> true,
				'msg'	=> null,
				'data'	=> $dataBantuan
			);
			echo json_encode($output);
		}
	}

	function SaveBansos(){

		$id_bansos = $this->input->post('id_bansos');
		$id_kpm = $this->input->post('id_kpm');
		$id_bantuan = $this->input->post('id_bantuan');
		$getLastId = $this->bm->getLastIdBansos()->lastid;
		
		if($id_bansos == -1){
			
			$dataInsert = array(
				'id_bansos'	=> $getLastId,
				'id_bantuan'	=> $id_bantuan,
				'id_kpm'	=> $id_kpm,
				'status_verifikasi'	=> 0,
				'created_by' => $this->session->userdata(S_ID_USER),
				'created_dt' => date('Y-m-d H:i:s')
			);

			$insertsBantuan = $this->bm->SaveBantuan($dataInsert,$id_bansos, 'bansos.tx_bansos');

			if($insertsBantuan){
				$output = array(
					'state'	=> true,
					'msg'	=> 'Data Bansos Berhasil Diinput'
				);
				echo json_encode($output);
			}else{
				$output = array(
					'state'	=> false,
					'msg'	=> 'Data Bansos gagal Diinput'
				);
				echo json_encode($output);
			}
		}
	}

	public function DownloadData(){

		
		/* $kode_kec = 3201021;
		$kode_desa = 0;
		$nik = ''; */

		$kode_kec = $this->input->get('kode_kec');
		$kode_desa = $this->input->get('kode_desa');
		$nik = $this->input->get('nik');
		
		$getData = $this->bm->download_data($kode_kec,$kode_desa,$nik);
		

		if(count($getData) != 0){
			$writer = WriterFactory::create(Type::XLSX);
	
			//stream to browser
			$writer->openToBrowser("data_bansos_".".xlsx");
	
			$header = [
				'No',
				'Nama',
				'Nik',
				'Kecamatan',
				'Desa',
				'Alamat',
				'Nama Bantuan',
			];
			$writer->addRow($header); // add a row at a time
			
			$data = array();
			$no = 1;
			foreach($getData as $dt){

				$dt = array(
					$no++,$dt->nama, $dt->nik, $dt->nama_kecamatan, $dt->nama_desa, $dt->alamat, $dt->nama_bantuan
				);
				array_push($data,$dt);
			}

			$writer->addRows($data); // add multiple rows at a time
	
			$writer->close();
		}
		
	}

	



}


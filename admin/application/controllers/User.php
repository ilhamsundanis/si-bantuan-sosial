<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('user_model','um');
	}
	public function index()
	{
	
		$data['jsapp'] = array('admin/user_pengguna');
		$this->load->view('header',$data);
		$this->load->view('user');
		$this->load->view('footer');
	}

	function getRoles(){
		$Roles = $this->um->getRole();
		$result['Data'] = $Roles;
		echo json_encode($result);
	}


	function get() {
        $order    = $this->input->post('order');
		$column   = $this->input->post('columns');
		$idx_cols = $order[0]['column'];

		$def = array(
            'draw' => $this->input->post('draw'),
            'length' => $this->input->post('length'),
            'order' => $column[$idx_cols]['name'],
            'start' => $this->input->post('start'),
            'dir' => $order[0]['dir']
        );
		
		
		$start = isset($_POST['start']) ? intval($_POST['start']) : 1;
		$length = isset($_POST['length']) ? intval($_POST['length']) : 5;
		
		

		$result = array();
        $recordsTotal = $this->um->count_data_user();
		
        $row = array();
		$results = $this->um->get_data($length,$start, $def['order'], 'asc');
        $dd = "";

        if (count($results) > 0) {
			$ii = $start;
			   
            foreach ($results as $d) {
				$ii++;
				
                $row[] = array
				(
					"no"				=> $ii,
					"username" 			=> $d->username,
					"email" 			=> $d->email,
					"fullName" 			=> $d->name,
					"role_name" 		=> $d->role_name,
				);
            }
        }
      
		$output = array
		(
			"draw"           => $def['draw'],
			"recordsTotal"   => $recordsTotal,
			"recordsFiltered"=> $recordsTotal,
			"data"           => $row
		);

		echo json_encode($output);
    }

	function SaveUser(){

		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$email = $this->input->post('email');
		$firstName = $this->input->post('firstName');
		$middleName = $this->input->post('middleName');
		$lastName = $this->input->post('lastName');
		$address = $this->input->post('address');
		$phoneNumber = $this->input->post('phoneNumber');
		$mobileNumber = $this->input->post('mobileNumber');
		$roles = $this->input->post('roles');
		
		$kodeWilayah = $this->input->post('kodeWilayah');

		$checkUser = $this->um->checkUserNameOrEmail($username,$email);
		if(count($checkUser) !=0 ){
			$arrResult = array(
				'state' => -1,
				'msg'   => 'Username or Email Sudah terdaftar'

			);
			$output = array
			(
				"data"           => $arrResult
			);
			echo json_encode($arrResult);
		}else{
			

		

			$data_insert = array(
				'username' => $username,
				'password' => $password,
				'email'	   => $email,
				'firstName' => $firstName,
				'middleName' => $middleName,
				'lastName'	=> $lastName,
				'address'	=> $address,
				'phoneNumber'	=> $phoneNumber,
				'mobileNumber'	=> $mobileNumber,
				'roles'	=> $roles,
				'kodeWilayah' => $kodeWilayah,
			);

			$inserUsers = $this->um->save_user($data_insert);
			$arrResult = array(
				'state' => 1,
				'msg'   => 'user berhasil diinput'

			);
			echo json_encode($arrResult);
		}

	}
}

<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');
		
class MY_Controller extends CI_Controller {
    
    var $_uri_string;
    
    function __construct() {
        parent:: __construct();
        $this->load->helper('error');
        $this->load->library('permission');
        $this->load->helper('common');
        
        $this->menu = $this->permission->get_access_menu();
        
    }
    
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: safari.erie@gmail.com
 * @created: 2020-13-11
 */
function show_401(){
    $exceptions =& load_class('Exceptions','core');
    $exceptions->show_401();
}

function show_error_custom($heading, $message, $code){
    $exceptions =& load_class('Exceptions','core');
    $exceptions->show_error_custom($heading, $message, $code);
}

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
 * @author: safari.erie@gmail.com
 * @created: 2020-13-11
 */

 class Permission{

	var $CI;// CI instance
    var $CID;
    var $UIdUser;
    var $page;
    var $_page;
    var $action;
	var $stdAction = array("create", "id", "edit", "delete", "approve", "profile","password");
	
	function __construct() {
        $this->CI = & get_instance();
        // set user_group and company id from session

        $this->UIdUser     = ($this->CI->session->userdata(S_ID_USER)) ? $this->CI->session->userdata(S_ID_USER) : 0;
      

        if ($this->UIdUser == null || $this->UIdUser == 0) {
            $this->CI->session->sess_destroy();
            redirect('admin/login');
        }
        
	}
	
	function get_user_permissions(){
		/* $this->CI->db->select('d.id_appl_task,d.id_appl_task_parent,d.id_appl,d.appl_task_name, d.controller_name,d.action_name,d.description,d.icon_name');
		$this->CI->db->from('public.tb_user_role a');
		$this->CI->db->join('public.tb_role b', 'a.id_role = b.id_role','inner'); */

		$sql = "
			select 
				d.id_appl_task,d.id_appl_task_parent,d.id_appl,d.appl_task_name,
				d.controller_name,d.action_name,d.description,d.icon_name
				from public.tb_user_role a
				inner join tb_role b on a.id_role = b.id_role
				inner join public.tb_role_appl_task c on a.id_role = c.id_role
				inner join public.tb_appl_task d on c.id_appl_task = d.id_appl_task
				inner join public.tb_appl e on d.id_appl = e.id_appl
			where e.id_appl = 1 and a.id_user = " .$this->UIdUser." 
		";

		$query = $this->CI->db->query($sql);
		if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }

	}

	function get_access_menu(){

		$sql = "
			select 
				d.id_appl_task,d.id_appl_task_parent,d.id_appl,d.appl_task_name,
				d.controller_name,d.action_name,d.description,d.icon_name
				from public.tb_user_role a
				inner join tb_role b on a.id_role = b.id_role
				inner join public.tb_role_appl_task c on a.id_role = c.id_role
				inner join public.tb_appl_task d on c.id_appl_task = d.id_appl_task
				inner join public.tb_appl e on d.id_appl = e.id_appl
			where e.id_appl = 1 and a.id_user = " .$this->UIdUser." 
		";

		$query = $this->CI->db->query($sql);
        if ($query->num_rows())
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
	}
 }

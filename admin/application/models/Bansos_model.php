
<?php


class Bansos_model extends CI_Model
{

	function get_data(
		$start = ''
		, $length = ''
	    , $order = ''
		, $dir = 'asc'
		, $kode_kec = ''
		, $kode_desa = ''
		, $nik = ''

	){
		$whereCondition;
		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		if($kodeWilayah == 0 || $kodeWilayah == null || $kodeWilayah == ''){
			$whereCondition = "WHERE 1 = 1";
		}else if(strlen($kodeWilayah) == 7){ // untuk kecamatan
			$whereCondition = "WHERE c.kode_kec = ".$kodeWilayah;
		}else{
			$whereCondition = "WHERE b.kode_desa = ".$kodeWilayah;
		}
		$sql = "
			SELECT 
				a.id_kpm, a.nama, a.nik, a.alamat, a.layak, a.status,a.dtks,
				f.id_bantuan,f.nama_bantuan,f.keterangan,f.status,
				a. kode_desa, b.name as nama_desa,
				c.kode_kec, c.name as nama_kecamatan,
				d.kode_kab,d.name as nama_kabupaten,
				e.kode_prov,e.name as nama_prov	
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			$whereCondition
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}
		
		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();
	
	}

	function count_data($kode_kec,$kode_desa,$nik){

		$whereCondition;
		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		if($kodeWilayah == 0 || $kodeWilayah == null || $kodeWilayah == ''){
			$whereCondition = "WHERE 1 = 1";
		}else if(strlen($kodeWilayah) == 7){ // untuk kecamatan
			$whereCondition = "WHERE c.kode_kec = ".$kodeWilayah;
		}
		$sql = "
			SELECT 
				count(*) as cnt
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			$whereCondition
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		return $this->db->query($sql)->row()->cnt;

	}


	function getKodeWilayahByIdUser(){
		$id_user = $this->session->userdata(S_ID_USER);

		$sql = "
			SELECT kode_wilayah from tb_user_profile 
			where id_user = $id_user;
		"; 

		$query = $this->db->query($sql)->row();
		return $query;
	}


	function getKpmByKodeWilayah(
		$start = ''
	   , $length = ''
	   , $order = ''
	   , $dir = 'asc'
	   , $nama = ''
   ){

	   $kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
	   $whereCondition;
	   if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
		   $whereCondition = " 1 = 1";
	   }
	   else if(strlen($kodeWilayah) == 7){
		   $whereCondition = "substring(cast (kode_desa as text), 0,8) = '$kodeWilayah'";
	   }else{
		   $whereCondition = "kode_desa = ".$kodeWilayah;
	   }

	   $sql = "
		   select id_kpm,kode_desa,nama,nik,alamat,layak,status,dtks,latitude,longitude
		   from bansos.tm_kpm a
		   where $whereCondition
			AND NOT EXISTS  (
				SELECT * 
				FROM bansos.tx_bansos b
				WHERE a.id_kpm = b.id_kpm
			)
	   ";

	   if($nama != ''){
		   $sql .= " AND c.nama LIKE '{$nama}'";
	   }

	   $sql .= " limit " . $start . " OFFSET " . $length;
	   return $this->db->query($sql)->result();

	   

   }


	function count_data_kpm($nama){

		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;

		$whereCondition;
		if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
			$whereCondition = " 1 = 1";
		}
		else if(strlen($kodeWilayah) == 7){
			$whereCondition = "substring(cast (kode_desa as text), 0,8) = '$kodeWilayah'";
		}else{
			$whereCondition = "kode_desa = ".$kodeWilayah;
		}
		$sql = "
			SELECT count(*) as cnt
			from bansos.tm_kpm a		
			where $whereCondition 
			and NOT EXISTS  (
				SELECT * 
				FROM bansos.tx_bansos b
				WHERE a.id_kpm = b.id_kpm
			)
		";
		if($nama != ''){
			$sql .= " AND c.nama = '{$nama}'";
		}
		return $this->db->query($sql)->row()->cnt;


	}


	function getBantuan(){
		$sql = "
			SELECT id_bantuan,nama_bantuan,keterangan,status from ref.tb_bantuan
		";

		$query = $this->db->query($sql);
		return $query->result();

	}


	function getLastIdBansos(){
		$sql = "
			select count(*) + 1 as  lastId from bansos.tx_bansos
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function SaveBantuan($dataBansos,$id_bansos,$table)
	{
		if($id_bansos == -1){
			$this->db->insert($table, $dataBansos);
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}
	}


	function download_data(
		 $kode_kec = ''
		, $kode_desa = ''
		, $nik = ''

	){
		$whereCondition;
		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		if($kodeWilayah == 0 || $kodeWilayah == null || $kodeWilayah == ''){
			$whereCondition = "WHERE 1 = 1";
		}else if(strlen($kodeWilayah) == 7){ // untuk kecamatan
			$whereCondition = "WHERE c.kode_kec = ".$kodeWilayah;
		}else{
			$whereCondition = "WHERE b.kode_desa = ".$kodeWilayah;
		}
		$sql = "
			SELECT 
				a.id_kpm, a.nama, a.nik, a.alamat, a.layak, a.status,a.dtks,
				f.id_bantuan,f.nama_bantuan,f.keterangan,f.status,
				a. kode_desa, b.name as nama_desa,
				c.kode_kec, c.name as nama_kecamatan,
				d.kode_kab,d.name as nama_kabupaten,
				e.kode_prov,e.name as nama_prov	
			from bansos.tm_kpm a
			INNER JOIN bansos.tx_bansos txb on a.id_kpm = txb.id_kpm 
			INNER JOIN ref.tb_bantuan f on txb.id_bantuan = f.id_bantuan 
			INNER JOIN master.tb_desa b on a.kode_desa = b.kode_desa
			INNER JOIN master.tb_kec c on b.kode_kec = c.kode_kec
			INNER JOIN master.tb_kab d on c.kode_kab = d.kode_kab
			INNER JOIN master.tb_prov e on d.kode_prov = e.kode_prov
			$whereCondition
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

        return $this->db->query($sql)->result();
	
	}

}

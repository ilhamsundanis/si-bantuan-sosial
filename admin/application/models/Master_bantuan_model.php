
<?php


class Master_bantuan_model extends CI_Model
{

	
	function getData(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $namaBantuan = ''
	){
		$sql = "
		SELECT id_bantuan,nama_bantuan,keterangan,status 
		FROM ref.tb_bantuan";

		if($namaBantuan != ''){
			$sql .= " AND nama_bantuan LIKE '{$namaBantuan}'";
		}

		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	}

	function count_data_bantuan($namaBantuan){
		$sql = "
		SELECT count(*) as cnt
		FROM ref.tb_bantuan";

		if($namaBantuan != ''){
			$sql .= " AND nama_bantuan LIKE '{$namaBantuan}'";
		}

		return $this->db->query($sql)->row()->cnt;
	}

	function getBantuan(){
		$sql = "
			SELECT id_bantuan,nama_bantuan,keterangan,status from ref.tb_bantuan
		";

		$query = $this->db->query($sql);
		return $query->result();

	}

	function saveBantuan($data,$tableName,$id_bantuan){

		if($id_bantuan == -1){

			$this->db->insert($tableName,$data );
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}else{
			$this->db->where('id_bantuan',$id_bantuan);
			$this->db->update($tableName,$data);
			$result;
			if (!$this->db->affected_rows()) {
				$result = false;
			} else {
				$result = true;
			}

			return $result;
		}
	}

	function GetDataBantuanById($id_bantuan){
		$sql = "
			SELECT
				 id_bantuan,nama_bantuan,keterangan,status 
			FROM ref.tb_bantuan
			WHERE id_bantuan = $id_bantuan
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function checkBansosBantuan($id_bantuan){
		$sql = "
			SELECT 
				id_bansos,id_bantuan 
			FROM bansos.tx_bansos where id_bantuan = $id_bantuan
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	function deleteBantuan($where,$tableName){
		$this->db->where($where);
		$this->db->delete($tableName);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}

		return $result;
	}


	function getLast(){
		$sql = "
			select count(*) + 1 as  lastId from ref.tb_bantuan
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}



}

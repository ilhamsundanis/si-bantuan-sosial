<?php


class Menu_model extends CI_Model
{
    

    public function show_menu()
    {

		$sql = "
			select 
				d.id_appl_task,d.id_appl_task_parent,d.id_appl,d.appl_task_name,
				d.controller_name,d.action_name,d.description,d.icon_name
				from public.tb_user_role a
				inner join tb_role b on a.id_role = b.id_role
				inner join public.tb_role_appl_task c on a.id_role = c.id_role
				inner join public.tb_appl_task d on c.id_appl_task = d.id_appl_task
				inner join public.tb_appl e on d.id_appl = e.id_appl
			where e.id_appl = 1 and a.id_user = " . $this->session->userdata(S_ID_USER) ." 
		";
		
		$query = $this->db->query($sql)->result_array();
        //$dataJson = json_decode($dataService,true);
        $menu = $this->part_menu($query);
        return $menu;
    }

    public function part_menu($data)
    {
        $this->part_menu = '';

        if ($data != "") {
            $i = 1;
            foreach ($data as $item) {
                if ($item['id_appl_task_parent'] == 0) {
                    if ($item['controller_name'] != "") {
                        $this->part_menu .= "<li>";
                        $this->part_menu .= "<a href=" . base_url() . $item['UrlLink'] . "> " . $item['Name'] . " </a>";
                    } else {
                        $this->part_menu .= "<li class='nav-item dropdown'>";
                        $this->part_menu .= "<a href='#' class='nav-link has-dropdown'> " . $item['appl_task_name'] . " </a>";
                        $this->menu($item['id_appl_task'], $data);
                        $this->part_menu .= "</li>";
                    }
                    $i++;
                }
            }
        }

        return $this->part_menu;
    }

    public function menu($parent = 0, $data)
    {
        
        $this->part_menu .= "<ul class='dropdown-menu'>";
        foreach ($data as $item) {
            if ($parent == $item['id_appl_task_parent']) {
                $has_child = $this->has_child($item['id_appl_task'], $data);
                if ($item['controller_name'] != "") {
                    $this->part_menu .= "<li>";
                    $this->part_menu .= "<a class='nav-link' href=" . base_url() . $item['controller_name'] . ">" . $item['appl_task_name'] . "</a>";
                    $this->part_menu .= "</li>";
                } else {
                    if ($has_child) {
                        $this->part_menu .= "<li class='dropdown-submenu'>";
                        $this->part_menu .= "<a href='#'>" . $item['appl_task_name'] . "</a>";
                        $this->menu($item['id_appl_task'], $get_url_service);
                        $this->part_menu .= "</li>";

                    }

                }

            }
        }
        $this->part_menu .= "</ul>";
    }

    public function has_child($parent, $data)
    {
        
        foreach ($data as $item) {
            if ($parent == $item['id_appl_task_parent']) {
                return true;
            }
        }
        return false;
    }

}

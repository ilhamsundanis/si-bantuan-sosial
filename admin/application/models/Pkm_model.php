<?php


class Pkm_model extends CI_Model
{
	
	function get_data(
		$start = ''
		, $length = ''
	    , $order = ''
	    , $dir = 'asc'
	    , $kode_kec = ''
		, $kode_desa  = ''
		, $nik = '' 

	){
		$sql = "
			SELECT a.id_kpm,a.kode_desa,a.nama,a.nik,a.alamat,a.status,a.dtks,
					b.kode_desa,b.name as nama_desa,
					c.kode_kec,c.name as nama_kec
			from bansos.tm_kpm a
			inner join master.tb_desa b on a.kode_desa = b.kode_desa
			inner join master.tb_kec c on b.kode_kec = c.kode_kec
			WHERE 1 = 1
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		
		
		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();
	
	}

	function count_data_pkm($kode_kec = ''
	, $kode_desa  = ''
	, $nik = ''){

		$sql = "
			SELECT 
				count(*) as cnt
				from bansos.tm_kpm a
				inner join master.tb_desa b on a.kode_desa = b.kode_desa
				inner join master.tb_kec c on b.kode_kec = c.kode_kec
			WHERE 1 = 1
		";

		if($kode_kec != 0 && $kode_desa == 0 && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}'";
		}

		if($kode_kec != 0 && $kode_desa != 0  && $nik == ''){
			$sql .= " AND c.kode_kec = '{$kode_kec}' AND b.kode_desa = '{$kode_desa}'";
		}

		if($kode_kec != 0 && $kode_desa != 0 && $nik != ''){
			$sql .= " AND c.kode_kec = {$kode_kec} AND b.kode_desa = {$kode_desa} AND a.nik = '{$nik}' ";
		}
		if($kode_kec == 0 && $kode_desa == 0  && $nik != ''){
			$sql .= " AND a.nik = '{$nik}'";
		}

		return $this->db->query($sql)->row()->cnt;

	}


	function SaveKpm($id_kpm,$dataKpm){

		if($id_kpm == -1){
			$this->db->insert('bansos.tm_kpm', $dataKpm);
			return $this->db->affected_rows() > 0;
		}else{

			$this->db->where('id_kpm',$id_kpm);
			$this->db->update('bansos.tm_kpm', $dataKpm);
			return $this->db->affected_rows() > 0;
		}
		
	}


	function getLastIdKpm(){
		$sql = "
			select count(*) + 1 as  lastId from bansos.tm_kpm
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getKpmById($idKpm){
		$sql = "
			SELECT a.id_kpm,a.kode_desa,a.nama,a.nik,a.alamat,a.layak,a.status,a.dtks,
					b.kode_kec
			from bansos.tm_kpm a
			inner join master.tb_desa b on a.kode_desa = b.kode_desa
			where id_kpm = $idKpm
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function checkBansos($id_kpm){

		$sql = "
			SELECT id_bansos,id_bantuan,id_kpm,status_verifikasi,keterangan
			FROM 
			bansos.tx_bansos where id_kpm = $id_kpm
		";

		$query = $this->db->query($sql);
		return $query->row();
	}

	function KpmDelete($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
		$result;
		if (!$this->db->affected_rows()) {
			$result = false;
		} else {
			$result = true;
		}

		return $result;
	}
	

}

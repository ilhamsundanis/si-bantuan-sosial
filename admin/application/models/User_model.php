<?php


class User_model extends CI_Model
{
	
	
	public function get_data($start = ''
	, $length = ''
	, $order = ''
	, $dir = 'asc'){

		$sql = "
			SELECT a.id_user,a.username,a.password,a.email,a.status,
				b.first_name,b.middle_name,b.last_name, concat(b.first_name,' ',b.middle_name,' ',b.last_name) as name,b.address,
				b.phone_number,b.mobile_number,
				d.role_name
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role

		";

		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();

	}

	function count_data_user($start = ''
	, $length = ''
	, $order = ''
	, $dir = 'asc')
	{
		
		$sql = "			
			SELECT count(*) as cnt
			from tb_user a
			inner join tb_user_profile b on a.id_user = b.id_user
			inner join tb_user_role c on a.id_user = c.id_user
			inner join tb_role d on c.id_role = d.id_role
		";

		return $this->db->query($sql)->row()->cnt;
	}

	public function getRole(){

		$sql = "
			SELECT 
			id_role,role_name 
			FROM public.tb_role
		";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function checkUserNameOrEmail($username,$email){
		
		$sql = "
			select username,email from tb_user
			where username = '$username' or email = '$email'
		";

		$query = $this->db->query($sql);
		return $query->result_array();

	}

	public function save_user($data_user){

		$lastIdUser = $this->getLastIdUser()->lastiduser;

		$dt_tbl_user = array(
			'id_user' => $lastIdUser,
			'username'	=> $data_user['username'],
			'password'	=> $data_user['password'],
			'email'		=> $data_user['email'],
			'status'	=> 1,
			'created_date' => date('Y-m-d H:i:s')
		);
		$inserUser = $this->db->insert('tb_user',$dt_tbl_user);

		$dt_tb_user_info = array(
			'id_user'	=> $lastIdUser,
			'first_name'	=> $data_user['firstName'],
			'middle_name'	=> $data_user['middleName'],
			'last_name'	=> $data_user['lastName'],
			'address'	=> $data_user['address'],
			'phone_number'	=> $data_user['phoneNumber'],
			'mobile_number'	=> $data_user['mobileNumber'],
			'kode_wilayah'	=> $data_user['kodeWilayah'],
		);

		$insertUserProfile = $this->db->insert('tb_user_profile', $dt_tb_user_info);

		$dt_user_role = array(
			'id_user' => $lastIdUser,
			'id_role' => $data_user['roles']
		);

		$insertUserRole = $this->db->insert('tb_user_role', $dt_user_role);
		$user_detail = array(
            'last_user' => $lastIdUser
        );

        return $user_detail; 
	}

	public function getLastIdUser(){

		$sql = "
			select max(id_user)+1 as lastIdUser from tb_user
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

}

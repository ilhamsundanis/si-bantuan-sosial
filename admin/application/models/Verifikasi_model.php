<?php


class Verifikasi_model extends CI_Model
{

	function getKpmByKodeWilayah(
		 $start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
		, $nama = ''
	){

		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;
		$whereCondition;
		if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
			$whereCondition = " 1 = 1";
		}
		else if(strlen($kodeWilayah) == 7){
			$whereCondition = "substring(cast (kode_desa as text), 0,8) = '$kodeWilayah'";
		}else{
			$whereCondition = "kode_desa = ".$kodeWilayah;
		}

		$sql = "
			select id_kpm,kode_desa,nama,nik,alamat,layak,status,dtks,latitude,longitude
			from bansos.tm_kpm
			where $whereCondition
		";

		if($nama != ''){
			$sql .= " AND c.nama = '{$nama}'";
		}

		$sql .= " limit " . $start . " OFFSET " . $length;
        return $this->db->query($sql)->result();

		

	}

	function count_data_kpm($nama){

		$kodeWilayah = $this->getKodeWilayahByIdUser()->kode_wilayah;

		$whereCondition;
		if($kodeWilayah == null || $kodeWilayah == 0 || $kodeWilayah == ''){
			$whereCondition = " 1 = 1";
		}
		else if(strlen($kodeWilayah) == 7){
			$whereCondition = "substring(cast (kode_desa as text), 0,8) = '$kodeWilayah'";
		}else{
			$whereCondition = "kode_desa = ".$kodeWilayah;
		}

		$sql = "
			SELECT count(*) as cnt
			from bansos.tm_kpm
			where $whereCondition
		";
		if($nama != ''){
			$sql .= " AND c.nama = '{$nama}'";
		}
		return $this->db->query($sql)->row()->cnt;


	}

	function getKodeWilayahByIdUser(){
		$id_user = $this->session->userdata(S_ID_USER);

		$sql = "
			SELECT kode_wilayah from tb_user_profile 
			where id_user = $id_user;
		"; 

		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getLastVerifikasi(){
		$sql = "
			select count(*) + 1 as  lastIdVerifikasi from bansos.tb_verifikasi
		";
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function saveVerifikasi($dataVerifikasi){

		$checkVerifikasi = $this->checkVerifikasi($dataVerifikasi['id_kpm']);

		if(count($checkVerifikasi) == 0){
			$this->db->insert('bansos.tb_verifikasi', $dataVerifikasi);
		}else{
			$updateVerifikasi = array(
				'hasil'	=> $dataVerifikasi['hasil'],
				'foto_ktp'	=> $dataVerifikasi['foto_ktp'],
				'foto_rumah'	=> $dataVerifikasi['foto_rumah'],
				'updated_by' => $this->session->userdata(S_ID_USER),
				'updated_dt' => date('Y-m-d H:i:s')
			);

			$this->db->where('id_verifikasi',$checkVerifikasi->id_verifikasi);
			$this->db->update('bansos.tb_verifikasi', $updateVerifikasi);

		}
		

		$dataUpdate = array(
			'status_verifikasi' => 1,
			'keterangan'	=> $dataVerifikasi['hasil']
		);
		$this->db->where('id_kpm',$dataVerifikasi['id_kpm']);
		$this->db->update('bansos.tx_bansos', $dataUpdate);

		return $this->db->affected_rows() > 0;

	}

	function checkVerifikasi($idVerifikasi){

		$sql = "select id_verifikasi,id_kpm, hasil
				from bansos.tb_verifikasi 
				where id_kpm = ".$idVerifikasi;
		$query = $this->db->query($sql)->row();
		return $query;
	}

	function getDataVerifikasi(
		$start = ''
		, $length = ''
		, $order = ''
		, $dir = 'asc'
	){

		$sql = "
			select a.id_verifikasi,a.id_kpm,a.hasil,a.foto_ktp,a.foto_rumah,
					b.id_bansos,b.status_verifikasi,b.keterangan,
					c.nama,c.nik,c.alamat,
					d.nama_bantuan
			from bansos.tb_verifikasi a
			inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
			inner join bansos.tm_kpm c on b.id_kpm = c.id_kpm
			inner join  ref.tb_bantuan d on b.id_bantuan = d.id_bantuan
		";

		$sql .= " limit " . $start . "OFFSET " . $length;
        return $this->db->query($sql)->result();

		
	}

	function countVerifikasi(){
		$sql = "
			select count(*) as cnt
			from bansos.tb_verifikasi a
			inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
			inner join bansos.tm_kpm c on b.id_kpm = c.id_kpm
			inner join  ref.tb_bantuan d on b.id_bantuan = d.id_bantuan
		";
		
		return $this->db->query($sql)->row()->cnt;
	}

}

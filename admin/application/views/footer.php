<footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2020 <div class="bullet"></div> Dinas Sosial Kabupaten Bogor
        </div>
       
      </footer>
    </div>
  </div>

  <!-- General JS Scripts -->
 <!--  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
  <script src="<?php echo base_url();?>assets/plugin/jquery/dist/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="<?php echo base_url();?>assets/admin/bootstrap/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/stisla.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
  <script src="<?php echo base_url();?>assets/admin/js/custom.js"></script>
	<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/jquery.dataTables.js"></script>  
	<script src="<?php echo base_url();?>assets/plugin/datatable/1.10.19/js/dataTables.bootstrap4.min.js"></script>  

	<script src="<?php echo base_url();?>assets/admin/amcharts3/amcharts.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/serial.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/radar.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/pie.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/plugins/animate/animate.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/plugins/export/export.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/light.js"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/black.js"></script>
	<script src="<?php echo base_url();?>assets/admin/amcharts3/themes/dark.js"></script>

  <!-- jsapp -->
<?php
if (isset($jsapp)) : foreach ($jsapp as $js) : ?>
    <script type="text/javascript" src="<?php echo base_url() ?>jsapp/<?php echo $js ?>.js"></script>
<?php
  endforeach;
endif;
?>
</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <title>Admin Sibos &mdash; Bogor</title>
  <!-- General CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/bootstrap/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css"/>
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/components.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/style-shared.css">
	<link href="<?php echo base_url(); ?>assets/plugin/datatable/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
	<script type="text/javascript">
    var SITE_URL = '<?php echo site_url() ?>admin/';

    //uri string ada di my_controller
    var CONTROLLER = '<?php echo ($this->uri->segment(1) !== FALSE) ? $this->uri->segment(1) : ""; ?>';

    // console.log('tes >> '+CONTROLLER);
	</script>
</head>

<body>
  <div id="app">
    <div class="main-wrapper main-wrapper-1">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
            <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
          </ul>
          
        </form>
        <ul class="navbar-nav navbar-right">
          
        
          <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="<?php echo base_url()?>assets/admin/img/avatar/avatar-1.png" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Eri Safari</div></a>
            <div class="dropdown-menu dropdown-menu-right">              
              <!-- <a href="features-profile.html" class="dropdown-item has-icon">
                <i class="far fa-user"></i> Profile
              </a> -->
            
              <div class="dropdown-divider"></div>
              <a href="<?php echo base_url();?>admin/login/out" class="dropdown-item has-icon text-danger">
                <i class="fas fa-sign-out-alt"></i> Logout
              </a>
            </div>
          </li>
        </ul>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand">
            <a href="<?php echo base_url();?>admin/main">Administrator SIBOS</a>
          </div>
          <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">SBS</a>
          </div>
          <ul class="sidebar-menu">
              <!-- <li class="menu-header">Dashboard</li>
              <li class="nav-item dropdown active">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                  <li class="active"><a class="nav-link" href="index-0.html">General Dashboard</a></li>
                 
                </ul>
              </li>
              <li class="menu-header">Konfigurasi Aplikasi</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-columns"></i> <span>Pengaturan</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="layout-default.html">Input Pengguna</a></li>
                  <li><a class="nav-link" href="layout-transparent.html">Input Menu Aplikasi</a></li>
                  <li><a class="nav-link" href="layout-top-navigation.html">Hak Akses Pengguna</a></li>
                </ul>
              </li>
              <li class="menu-header">Operator</li>
              <li class="nav-item dropdown">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-th-large"></i> <span>Bansos</span></a>
                <ul class="dropdown-menu">
                  <li><a class="nav-link" href="components-article.html">Input Penerima Bantuan</a></li>
                  <li><a class="nav-link beep beep-sidebar" href="components-avatar.html">Pengaduan</a></li>
                </ul>
              </li> -->
			  <?php
				
						$ci =&get_instance();
						$ci->load->model('M_menu');
						$menu = $ci->M_menu->show_menu(); 
						echo $menu;
						?>
							<!-- <a href="<?php echo base_url();?>admin/login/out" class="nav-link has-dropdown"><i class="fas fa-sign-out-alt"></i><span> Logout</span></a> -->
					
            </ul>
        </aside>
      </div>

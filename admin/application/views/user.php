
  <div class="main-content">
  <section class="section">
    <div class="section-header">
        <h1>Manajemen User</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a asp-controller="Home" asp-action="Index">Utility</a></div>
            <div class="breadcrumb-item">Manajemen Users</div>
        </div>
    </div>

    <div class="section-body">
        <div class="card">
            <div class="card-header">
                <h4 class="col-md-6">Data Users</h4>
                <h4 class="col-md-6 text-right"><button type="button" class="btn btn-primary" onclick="AddUser();"><i class="fa fa-plus"></i> Tambah User</button></h4>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table id="TabelManajemenUser" class="table table-striped table-bordered table-hover nowrap" style="width:100%;">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Nama</th>
                                <th>Sebagai</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- modal here -->
<div class="modal fade" id="ModalFormUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="TitleFormUser">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" action="#" enctype="multipart/form-data" id="FormUser">
                    <input type="hidden" class="form-control" name="IdUser" placeholder="Ketikkan Id User..." />
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Daftar Sebagai:</label>
                        <div class="col-sm-9">
                            <select name="roles" id="roles" class="form-control">
                            </select>
                        </div>
                    </div>
					<div class="form-group form-row" id="Divkecamatan" style="display:none;">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Kecamatan</label>
                        <div class="col-sm-9">
                            <select name="kecamatan" id="kecamatan" class="form-control">
                            </select>
                        </div>
                    </div>
					<div class="form-group form-row" id="Divdesa" style="display:none;">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Desa</label>
                        <div class="col-sm-9">
                            <select name="desa" id="desa" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Username:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="username" id="username" placeholder="Ketikkan Username..." />
                        </div>
                    </div>
                    <div class="form-group form-row" id="DivPassword">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Password:</label>
                        <div class="col-sm-9">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Ketikkan Password..." />
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Email:</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Ketikkan Email..." />
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Nama Lengkap:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Nama Depan..." />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="middleName" id="middleName" placeholder="Nama Tengah..." />
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" name="lastName" id="lastName" placeholder="Nama Belakang..." />
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Alamat Lengkap:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="address" id="address" placeholder="Ketikkan Alamat Lengkap..."></textarea>
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Nomor Telpon:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="PhoneNumber" id="phoneNumber" placeholder="Ketikkan Nomor Telpon..." />
                        </div>
                    </div>
                    <div class="form-group form-row">
                        <label class="col-form-label col-sm-3" style="text-align:left;">Nomor HP:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="MobileNumber" id="mobileNumber" placeholder="Ketikkan Nomor HP..." />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Tutup</button>
                <button type="button" class="btn btn-primary" id="BtnSaveUser"><i class="fa fa-save"></i> Simpan Data</button>
            </div>
        </div>
    </div>
</div>
</div>

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$data['jsapp'] = array('FrontEnd/main_landing');
		$this->load->view('header',$data);
		$this->load->view('main');
		$this->load->view('footer');
	}
}

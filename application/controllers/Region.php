<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Region extends CI_Controller {

	/* public function index()
	{
		$data['jsapp'] = array('FrontEnd/main_landing');
		$this->load->view('header',$data);
		$this->load->view('main');
		$this->load->view('footer');
	} */

	function __construct() {
        parent:: __construct();
        $this->load->model('region_statistik_model','rsm');
	}

	public function typeBntn(){

		$data['count_presiden_all'] = $this->rsm->countAllBansosPresiden();
		$data['count_dtks_presiden'] = $this->rsm->countDtksBansosPresiden();
		$data['count_non_dtks_presiden'] = $this->rsm->countNonDtksBansosPresiden();

		$data['count_prov_all'] = $this->rsm->countAllBansosProv();
		$data['count_dtks_prov'] = $this->rsm->countDtksBansosProv();
		$data['count_non_dtks_prov'] = $this->rsm->countNonDtksBansosProv();

		$data['count_bupati_all'] = $this->rsm->countAllBansosBupati();
		$data['count_dtks_bupati'] = $this->rsm->countDtksBansosBupati();
		$data['count_non_dtks_bupati'] = $this->rsm->countNonDtksBansosBupati();

		$data['count_dd_all'] = $this->rsm->countAllBansosDd();
		$data['count_dtks_dd'] = $this->rsm->countDtksBansosDd();
		$data['count_non_dtks_dd'] = $this->rsm->countNonDtksBansosDd();

		$data['count_reguler_all'] = $this->rsm->countAllBansosReguler();
		$data['count_dtks_reguler'] = $this->rsm->countDtksBansosReguler();
		$data['count_non_dtks_reguler'] = $this->rsm->countNonDtksBansosReguler();
		
		$data['count_Perluasan_all'] = $this->rsm->countAllBansosPerluasan();
		$data['count_dtks_Perluasan'] = $this->rsm->countDtksBansosPerluasan();
		$data['count_non_dtks_Perluasan'] = $this->rsm->countNonDtksBansosPerluasan();
		
		$data['count_pkh_all'] = $this->rsm->countAllBansosPkh();
		$data['count_dtks_Pkh'] = $this->rsm->countDtksBansosPkh();
		$data['count_non_dtks_Pkh'] = $this->rsm->countNonDtksBansosPkh();

		$this->load->view('header',$data);
		$this->load->view('typeBntn');
		$this->load->view('footer');
	}

	public function typeByArea(){

		$data['Count_kec'] = $this->rsm->countAllBantuanKecamatan();
		$this->load->view('header',$data);
		$this->load->view('typeByArea');
		$this->load->view('footer');
	}

	public function areaDet(){
		$getKodeKec = $this->input->get('kdArea');
		
		$data['count_presiden_all'] = $this->rsm->countAreaBansosPresidenAll($getKodeKec);
		$data['count_presiden_dtks'] = $this->rsm->countAreaBansosPresidendtks($getKodeKec);
		$data['count_presiden_non_dtks'] = $this->rsm->countAreaBansosPresidenNondtks($getKodeKec);
		
		$data['count_prov_all'] = $this->rsm->countAreaBansosProvAll($getKodeKec);
		$data['count_prov_dtks'] = $this->rsm->countAreaBansosProvdtks($getKodeKec);
		$data['count_prov_non_dtks'] = $this->rsm->countAreaBansosProvNondtks($getKodeKec);

		$data['count_bupati_all'] = $this->rsm->countAreaBansosBupatiAll($getKodeKec);
		$data['count_bupati_dtks'] = $this->rsm->countAreaBansosBupatidtks($getKodeKec);
		$data['count_bupati_non_dtks'] = $this->rsm->countAreaBansosBupatiNondtks($getKodeKec);

		$data['count_dana_desa_area_all'] = $this->rsm->countAreaBansosDanaDesaAll($getKodeKec);
		$data['count_dana_desa_araa_dtks'] = $this->rsm->countAreaBansosDanaDesadtks($getKodeKec);
		$data['count_dana_desa_araa__non_dtks'] = $this->rsm->countAreaBansosDanaDesaNondtks($getKodeKec);

		$data['count_sr_all'] = $this->rsm->countAreaBansosSrAll($getKodeKec);
		$data['count_sr_dtks'] = $this->rsm->countAreaBansosSrdtks($getKodeKec);
		$data['count_sr_non_dtks'] = $this->rsm->countAreaBansosSrNondtks($getKodeKec);


		$data['count_perluasan_area_all'] = $this->rsm->countAreaBansosPerluasanAll($getKodeKec);
		$data['count_perluasan_area_dtks'] = $this->rsm->countAreaBansosPerluasandtks($getKodeKec);
		$data['count_perluasan_area_non_dtks'] = $this->rsm->countAreaBansosPerluasanNondtks($getKodeKec);
		
		$data['count_tunai_area_all'] = $this->rsm->countAreaBansosTunainAll($getKodeKec);
		$data['count_tunai_area_dtks'] = $this->rsm->countAreaBansosTunaindtks($getKodeKec);
		$data['count_tunai_area_non_dtks'] = $this->rsm->countAreaBansosTunainNondtks($getKodeKec);
		
		$data['count_pkh_area_all'] = $this->rsm->countAreaBansosPkhnAll($getKodeKec);
		$data['count_pkh_area_dtks'] = $this->rsm->countAreaBansosPkhndtks($getKodeKec);
		$data['count_pkh_area_non_dtks'] = $this->rsm->countAreaBansosPkhnNondtks($getKodeKec);

		$this->load->view('header',$data);
		$this->load->view('areaDet');
		$this->load->view('footer');
	}
}


<?php

class region_statistik_model extends CI_Model
{
	
	function countAllBansosPresiden(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 1;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosPresiden(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 1 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosPresiden(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 1 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}

	function countAllBansosProv(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 2;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosProv(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 2 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosProv(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 2 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBansosBupati(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 3;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosBupati(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 3 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosBupati(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 3 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBansosDd(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 4;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosDd(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 4 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosDd(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 4 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBansosReguler(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 5;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosReguler(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 5 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosReguler(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 5 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBansosPerluasan(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 6;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosPerluasan(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 6 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosPerluasan(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 6 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBansosTunai(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 7;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosTunai(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 7 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosTunai(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 7 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}

	function countAllBansosPkh(){
		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			where a.id_bantuan = 8;
		";

		return $this->db->query($sql)->row()->cnt;

	}

	function countDtksBansosPkh(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 8 and b.dtks = true";
		return $this->db->query($sql)->row()->cnt;
	}

	
	function countNonDtksBansosPkh(){
		$sql = " select count(*) as cnt
		from bansos.tx_bansos a
		inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
		where a.id_bantuan = 8 and b.dtks = false";
		return $this->db->query($sql)->row()->cnt;
	}


	function countAllBantuanKecamatan(){

		$sql = "
				select d.name as nama_kecamatan,d.kode_kec, count(a.id_kpm) as cnt
				from bansos.tm_kpm a
				inner join bansos.tx_bansos b on a.id_kpm = b.id_kpm
				inner join master.tb_desa c on a.kode_desa = c.kode_desa
				inner join master.tb_kec d on c.kode_kec = d.kode_kec
				where substring(cast(a.kode_desa as text), 0,8) = substring(cast(d.kode_kec as text), 0,8)
				group by d.name,d.kode_kec
		";

		$query = $this->db->query($sql)->result();
		return $query;

	}


	function countAreaBansosPresidenAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 1 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPresidendtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 1 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPresidenNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 1 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}


	function countAreaBansosProvAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 2 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosProvdtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 2 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosProvNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 2 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}


	
	function countAreaBansosBupatiAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 3 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosBupatidtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 3 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosBupatiNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 3 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}


	function countAreaBansosDanaDesaAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 4 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosDanaDesadtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 4 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosDanaDesaNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 4 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}



	function countAreaBansosSrAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 5 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosSrdtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 5 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosSrNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 5 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}


	function countAreaBansosPerluasanAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 6 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPerluasandtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 6 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPerluasanNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 6 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}



	function countAreaBansosTunainAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 7 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosTunaindtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 7 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosTunainNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 7 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}



	function countAreaBansosPkhnAll($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 8 
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPkhndtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 8 and  b.dtks = true
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}

	function countAreaBansosPkhnNondtks($kodeKecamatan){

		$sql = "
			select count(*) as cnt
			from bansos.tx_bansos a
			inner join bansos.tm_kpm b on a.id_kpm = b.id_kpm
			inner join master.tb_desa c on b.kode_desa = c.kode_desa
			where a.id_bantuan = 8 and  b.dtks = false
			and substring(cast(c.kode_desa as text), 0,8) = '$kodeKecamatan';
		";
		return $this->db->query($sql)->row()->cnt;


	}



}


<style>
	.text-gray-800 {
		color: #2d3748;
	}

	.text-lg {
		font-size: 1.125rem;
	}

	.font-bold {
			font-weight: 700;
	}

	.text-gray-500 {
		color: #a0aec0;
	}

	.text-sm {
		font-size: .875rem;
	}

	.text-sw-green {
		color: #27ae60;
	}

	.text-gray-600 {
		color: #757575;
	}

	.py-3 {
		padding-top: .75rem;
		padding-bottom: .75rem;
	}

	.text-gray-800 {
		color: #2d3748;
	}
	.mt-3 {
		margin-top: .75rem;
	}
	.italic {
		font-style: italic;
	}

	.text-red-500 {
		color: #f56565;
	}
	.text-xs {
		font-size: .75rem;
	}
</style>

<section id="contact" class="contact">
      <div class="container">

        <div class="section-title mt-4" data-aos="fade-up">
       
          <p>Form Pengaduan</p>
        </div>
		<div class="row">
			<div class="col-lg-5 align-items-stretch" data-aos="fade-up" data-aos-delay="100">
				
			</div>
		</div>
        <div class="row">
          <div class="col-lg-5  align-items-stretch" data-aos="fade-up" data-aos-delay="100">
		  		<p class="text-lg text-gray-800 font-bold">
					Data orang yang akan diadukan
				</p>
				<p class="text-sm text-gray-500 mt-1">
					Di bawah ini adalah data pribadi orang yang akan Anda adukan. Mohon
					untuk memastikan kembali setiap data berikut.
				</p>
				<div class="info">
					<div class="font-bold text-gray-800 mb-3">
						Identitas
					</div>
					<div>
						<div class="text-sm font-bold text-sw-green">NIK</div> 
						<div class="text-sm text-gray-600"><?php echo substr_replace($dt_kpm->nik, '..XXX', -3, -1)?></div>
					</div>
					<div class="mt-4">
						<div class="text-sm font-bold text-sw-green">Nama Lengkap</div> 
						<div class="text-sm text-gray-600"><?php echo substr_replace($dt_kpm->nama, '..XXX', -3, -1)?></div>
					</div>
					<div class="text text-gray-800 font-bold py-3 mt-3">
						Alamat
					</div>
					<div>
						<div class="text-sm font-bold text-sw-green">Alamat Lengkap</div> 
						<div class="text-sm text-gray-600">
						<?php echo $dt_kpm->alamat?>
						</div>
					</div>

					<div class="mt-4">
						<div class="text-sm font-bold text-sw-green">Provinsi</div> 
						<div class="text-sm text-gray-600">
							<?php echo $dt_kpm->nama_prov?>
						</div>
					</div>
					<div class="mt-4">
						<div class="text-sm font-bold text-sw-green">Kabupaten/Kota</div> 
						<div class="text-sm text-gray-600">
							<?php echo $dt_kpm->nama_kabupaten?>
						</div>
					</div>

					<div class="mt-4">
						<div class="text-sm font-bold text-sw-green">Kecamatan</div> 
						<div class="text-sm text-gray-600"> <?php echo $dt_kpm->nama_kecamatan?></div>
					</div>
					<div class="mt-4">
						<div class="text-sm font-bold text-sw-green">Desa/Kelurahan</div> 
						<div class="text-sm text-gray-600">
						<?php echo $dt_kpm->nama_desa?>
						</div></div>
				</div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 align-items-stretch" data-aos="fade-up" data-aos-delay="200">
			<p class="text-lg text-gray-800 font-bold">
				Data Pribadi Pelapor
			</p>
			<p class="text-xs text-gray-500 mt-1 mb-5">
				Pastikan untuk melengkapi semua data di bawah ini dengan lengkap.
			</p>
            <form action="" id="formPengaduan" method="post" role="form" class="">
				<input type="hidden" name="id_kpm_terlapor" id="id_kpm_terlapor" value="<?php echo $dt_kpm->id_kpm;?>">
              <div class="form-row">
                <div class="form-group col-md-12">
                  <label for="nik">Nik <span class="font-normal italic">(wajib diisi)</span> </label>
                  <input type="text" name="nik" class="form-control" id="nik" data-rule="minlen:30" placeholder="Masukan Nik" data-msg="Please enter at least 30 chars" />
                  <div class="validate"></div>
                </div>
              </div>
			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Nama Lengkap <span class="font-normal italic">(wajib diisi)</span></label>
					<input type="text" class="form-control" name="fullName" id="fullName" placeholder="Masukan Nama Lengkap" />
					<div class="validate"></div>
                </div>
              </div>

			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Nomor Telepon <span class="font-normal italic">(wajib diisi)</span></label>
					<input type="text" class="form-control" name="NoTlp" id="NoTlp" placeholder="Masukan No Telepon" />
					<div class="validate"></div>
                </div>
              </div>
              
              
			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Alamat <span class="font-normal italic">(wajib diisi)</span></label>
					<textarea class="form-control" name="alamat" id="alamat" rows="3" data-rule="required" data-msg="alamat" placeholder="Masukan alamat lengkap disertai dengan nomor rumah, RT, RW dan Kode Pos"></textarea>
					<p class="text-red-500 text-xs italic mt-1">
						harus diisi
						</p>
					<div class="validate"></div>
				</div>
              </div>

			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Kecamatan <span class="font-normal italic">(wajib diisi)</span></label>
					<select name="kecamatan" class="form-control" id="kecamatans">
						<?php
								echo '<option value="0"  > Pilih Kecamatan</option>';
								foreach ($dt_kec as $dc) {
									echo '<option value="' . $dc['kode_kec'] . '"  >' . $dc['name'] . '</option>';
								}
						?>
					</select>
					<p class="text-red-500 text-xs italic mt-1">
						harus diisi
						</p>
					<div class="validate"></div>
				</div>
              </div>

			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Desa <span class="font-normal italic">(wajib diisi)</span></label>
					<select name="desa" class="form-control" id="desas">
							<option value="0">--Pilih Desa--</option>
					</select>
					<p class="text-red-500 text-xs italic mt-1">
						harus diisi
						</p>
					<div class="validate"></div>
				</div>
              </div>

			  <div class="form-row">
                <div class="form-group col-md-12">
					<label for="name">Alasan Pengaduan <span class="font-normal italic">(wajib diisi)</span></label>

					<div class="col-md-5">
					<div class="form-check">
						<input class="form-check-input" type="radio" name="RadioKeterangan" value="layak" id="RadioKeterangan1" checked="">
						<label class="form-check-label" for="RadioKeterangan1">
							Layak
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="RadioKeterangan" value="meninggal" id="RadioKeterangan2">
						<label class="form-check-label" for="RadioKeterangan2">
							Meninggal
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="RadioKeterangan" value="pindah" id="RadioKeterangan3" >
						<label class="form-check-label" for="RadioKeterangan3">
							Pindah
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="RadioKeterangan" value="Tidak Mampu" id="RadioKeterangan4" >
						<label class="form-check-label" for="RadioKeterangan4">
							Tidak Mampu
						</label>
					</div>
					<div class="form-check">
						<input class="form-check-input" type="radio" name="RadioKeterangan" value="menerima bantuan lain" id="RadioKeterangan5" >
						<label class="form-check-label" for="RadioKeterangan5">
							Sudah Menerima Bantuan Lain
						</label>
					</div>
			  </div> 

			
			  <div class="form-row pt-2">
			  		<div class="text-center">
					  <button type="button" class="btn btn-warning" id="batal" onclick="BatalPengaduan()">   Batal </button>
					  <button type="button" class="btn btn-info" id="btn_pengaduan" onclick="SavePengaduan()">   Laporkan </button>
					    <!-- <button type="submit">Batal</button>
					  	<button type="submit">Berikutnya</button> -->
					</div>
			  </div>
             		 
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Us Section -->

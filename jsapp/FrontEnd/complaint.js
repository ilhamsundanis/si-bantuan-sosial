

const acces_url = {
	
	GetDesaByCodeKec : SITE_URL+"wilayah/getDesa",
	SavePengaduan : SITE_URL+"Complaint/savePengaduan",
}

$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});



function SavePengaduan(){
	var formData = new FormData($('#formPengaduan')[0]);
	let id_kpm = $('#id_kpm_terlapor').val();
	let nik_pelapor = $('#nik').val();
	let fullName = $('#fullName').val();
	let noTlp = $('#NoTlp').val();
	let alamat = $('#alamat').val();
	let kecamatans = $('#kecamatans').val();
	let desas = $('#desas').val();
	let keterangan = $('input[name="RadioKeterangan"]:checked').val();
	console.log(fullName);
	if(nik_pelapor.length < 16){
		alert('Nik harus 16 digit')
	}else if(desas == 0){
		alert('desa belum dipilih')
	}else{
		var formData = new FormData($('#formPengaduan')[0]);
		formData.append('id_desa',desas);
		formData.append('nik_pengadu',nik_pelapor);
		formData.append('nama_pengadu',fullName);
		formData.append('alamat',alamat);
		formData.append('id_jenis_pengaduan',keterangan);		
		formData.append('id_kpm',id_kpm);
		formData.append('no_tlp',noTlp);

		$.ajax({
			url:acces_url.SavePengaduan,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				$("body").css("cursor", "progress");
				$("#loading").removeAttr("style");
			},
			success:function(response) {
			   $("body").css("cursor", "default");
			   if(response.state != true){
					alert("data gagal diinput");
			   }else{
					alert('Pengaduan Berhasil diinput');
					window.location = SITE_URL + 'search';
			   }
				
			}
		})
	}


}

function BatalPengaduan(){
	window.location = SITE_URL + 'search';
}

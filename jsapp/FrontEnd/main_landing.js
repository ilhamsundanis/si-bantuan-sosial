

const acces_url = {
	GetDataKecamatan : SITE_URL+"wilayah/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"wilayah/getDesa",
	GetBantuan : SITE_URL+"wilayah/getBantuan",
}

$(document).ready(function(){
	GetKecamatan();
	GetBantuan();
});

GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}

function toBantuan(){
	window.location = SITE_URL + 'Region/typeBntn';
}

function toArea(){
	window.location = SITE_URL + 'Region/typeByArea';
}

$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});


$("#kecamatan").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});

GetBantuan = () =>
{
	$.ajax({
		url: acces_url.GetBantuan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.id_bantuan + "' >" + v.nama_bantuan + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='jenis_bantuan']").empty().append(html);
		}
	})	
}

$('#FilterDetail').on('click',()=>{
	$('#ModalFilterAdvance').modal('show');
	GetKecamatan();
	GetBantuan();
});


$('#terapkan').on('click',function(){

	let kec = $('#kecamatans').val();
	let desa = $('#desas').val();
	let bantuanId = $('#jenisBantuans').val();
	/* console.log("=> "+kec);
	console.log("=> "+desa);
	console.log("=> "+bantuanId); */

	let param;
	if(kec != 0){
		if(desa != 0){			
			if(bantuanId != 0){
				param = 'kode_kec='+kec+'&kode_desa='+desa+"&bantuan="+bantuanId;
			}else{
				param = 'kode_kec='+kec+'&kode_desa='+desa;
			}
		}else{
			if(bantuanId != 0 ){
				param = 'kode_kec='+kec+'&bantuan='+bantuanId;
			}else{
				param = 'kode_kec='+kec;
			}
		}

	}else {
		if(bantuanId != 0 ){
			param = 'bantuan='+bantuanId;
		}
	}

	
	if(typeof param === 'undefined'){
		window.location = SITE_URL + 'search';
	}else{
		window.location = SITE_URL + 'search?'+param
	}

	

});


$('#terapkanFilter').on('click',function(){

	let kec = $('#kecamatan').val();
	let desa = $('select[name="desa"] option:selected').val();
	
	let bantuanId = $('#bantuanId').val();
	
/* 	console.log("=> "+kec);
	console.log("=> "+desa);
	console.log("=> "+bantuanId);  */

	let param;
	if(kec != 0){
		if(desa != 0){			
			if(bantuanId != 0){
				param = 'kode_kec='+kec+'&kode_desa='+desa+"&bantuan="+bantuanId;
			}else{
				param = 'kode_kec='+kec+'&kode_desa='+desa;
			}
		}else{
			if(bantuanId != 0 ){
				param = 'kode_kec='+kec+'&bantuan='+bantuanId;
			}else{
				param = 'kode_kec='+kec;
			}
		}

	}else {
		if(bantuanId != 0 ){
			param = 'bantuan='+bantuanId;
		}
	}

	
	if(typeof param === 'undefined'){
		window.location = SITE_URL + 'search';
	}else{
		window.location = SITE_URL + 'search?'+param
	} 

});

$('#btnCariNik').on('click',function(){
	var nik = $('#nik').val();
	var nLength = nik.length;
	
	if(nLength < 16){
		alert('pencarian nik harus 16 digit')
	}else{
		let param = '';
		param = "nik="+nik;
		window.location = SITE_URL + 'search?'+param;
	}

	
});




var oTable;

const acces_url = {
	GetDataDesa : SITE_URL+"MasterDesa/get",
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	SaveDesa : SITE_URL+"MasterDesa/SaveDesa",
}
$(document).ready(function(){
	
	oTable = $("#tableMasterDesaContent").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": acces_url.GetDataDesa,
			"type": "POST",
			"datatype": "json",
			/* "data": function (d) {      				


				d.kode_kec   = $('#Desas').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            } */
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "namaKec", "name": "namaKec", orderable: true,className: 'text-center' },
			{ "data": "namaDesa", "namaDesa": "nama", orderable: true,className: 'text-center' },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});

function searchdata(){
	oTable.ajax.reload();
}

function clear(){
	$('#kecamatan').val(0);
	$('#kode_desa').val('');
	$('#nama_desa').val('');

}

function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';
}


GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}


function AddMasterDesa  () {
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';
	GetKecamatan();
}

function onSubmitdesa(){
	let kode_kec = $('#kecamatan').val();
	let kode_desa = $('#kode_desa').val();
	let nama_desa = $('#nama_desa').val();
	let id_desa = $('#id_desa').val();
	if(kode_kec == 0){
		alert('Kecamatan Belum dipilih');
	}else if(kode_desa.length < 10){
		alert('Kode Desa kurang dari 10 digit');
	}else {
		var formData = new FormData($('#formdesa')[0]);
		formData.append('id_desa',id_desa);
		formData.append('kode_kec',kode_kec);
		formData.append('kode_desa',kode_desa);
		formData.append('nama_desa',nama_desa);

		$.ajax({
			url:acces_url.SaveDesa,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				$("body").css("cursor", "progress");
				$("#loading").removeAttr("style");
			},
			success:function(response) {
				$("body").css("cursor", "default");
			   if(response.state != true){
					alert(response.msg);
			   }else{
					alert(response.msg);
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
					clear();
			   }
			}
		})


	}
}


var oTable;

const acces_url = {
	GetDataKecamatan : SITE_URL+"MgtKpm/getKecamatan",
	GetDesaByCodeKec : SITE_URL+"MgtKpm/getDesa",
	GetBantuan : SITE_URL+"MgtKpm/getBantuan",
	SaveKpm : SITE_URL+"MgtKpm/SaveKpm",
	GetKpmById : SITE_URL+"MgtKpm/GetKpmById",
	GetKpmDataDelete : SITE_URL+"MgtKpm/GetKpmDataDelete",
	KpmDelete : SITE_URL+"MgtKpm/KpmDelete",
}
$(document).ready(function() {

	GetKecamatan();
	
	
	oTable = $("#datatable").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "MgtKpm/get",
			"type": "POST",
			"datatype": "json",
			"data": function (d) {      				


				d.kode_kec   = $('#kecamatans').val();//3201161
				d.kode_desa  = $('#desas').val();
				d.nik 	 = $('#nik').val();
            }
		},
	
		"columns": [		
			
			{ "data": "no", "name": "no", orderable: true ,className: 'text-center'},
			{ "data": "nama", "name": "nama", orderable: true,className: 'text-center' },
			{ "data": "nik", "name": "nik", orderable: true ,className: 'text-center'},
			{ "data": "alamat", "name": "alamat", orderable: true,className: 'text-center' },
			{ "data": "kecamatan", "name": "kecamatan", orderable: false },
			{ "data": "desa", "name": "desa", orderable: false },
			{ "data": "status", "name": "status", orderable: false },
			{ "data": "status_dtks", "name": "status_dtks", orderable: false },
			{ "data": "aksi", "name": "aksi", orderable: false },
		]
	   
	});
});


GetKecamatan = () =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='kecamatan']").empty().append(html);
		}
	})	
}



$("#kecamatans").on('change',function(){
	let kdKec = this.value;
	$.ajax({
		url:acces_url.GetDesaByCodeKec,
		type: "POST",
		dataType : "json",
		data: {"kode_kec":kdKec},
		beforeSend: () => {

		},
		success: (response) => {
			let countData = response.Data.length;
			let html = '';
			if(countData > 0){				
				html += '<option value="0"> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {					
					html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='desa']").empty();
			$("select[name='desa']").append(html);
		}
	})

});

function searchdata(){
	oTable.ajax.reload();
   
}


	
GetInputKecamatan = (kodeKec) =>
{
	$.ajax({
		url: acces_url.GetDataKecamatan,
		method:"GET",
		dataType: "json",
		beforeSend:()=>{

		},
		success:(response)=>{
			let countData = response.Data.length;

			let html = '';
			if(countData > 0){		
				html += '<option value="0"> Pilih Salah Satu </option>';		
				$.each(response.Data,(i,v) => {					
					if(v.kode_kec == kodeKec){
						html += "<option value='" + v.kode_kec + "' selected >" + v.name + "</option>";
					}else{
						
						html += "<option value='" + v.kode_kec + "' >" + v.name + "</option>";
					}
				});

			}else{
				html += '<option value="-1"> Data tidak ada </option>';
			}
			$("select[name='inputKec']").empty().append(html);
		}
	})	
}

function AddKpm(){
	
	document.getElementById("ListData").style.display = 'none';
	document.getElementById("formInput").style.display = '';

	GetInputKecamatan('');

	$("#inputKec").on('change',function(){
		let kdKec = this.value;
		$.ajax({
			url:acces_url.GetDesaByCodeKec,
			type: "POST",
			dataType : "json",
			data: {"kode_kec":kdKec},
			beforeSend: () => {

			},
			success: (response) => {
				let countData = response.Data.length;
				let html = '';
				if(countData > 0){				
					html += '<option value="0"> Pilih Salah Satu </option>';
					$.each(response.Data,(i,v) => {					
						html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
					});

				}else{
					html += '<option value="-1"> Data tidak ada </option>';
				}
				$("select[name='inputDesa']").empty();
				$("select[name='inputDesa']").append(html);
			}
		});

	});

	
}

function getDataDesa(kdKec,kdDesa)
{

		$.ajax({
			url:acces_url.GetDesaByCodeKec,
			type: "POST",
			dataType : "json",
			data: {"kode_kec":kdKec},
			beforeSend: () => {

			},
			success: (response) => {
				let countData = response.Data.length;
				let html = '';
				if(countData > 0){				
					html += '<option value="0"> Pilih Salah Satu </option>';
					$.each(response.Data,(i,v) => {		
						if(v.kode_desa == kdDesa){
							html += "<option value='" + v.kode_desa + "' selected>" + v.name + "</option>";
						}else{							
							html += "<option value='" + v.kode_desa + "' >" + v.name + "</option>";
						}			
					});

				}else{
					html += '<option value="-1"> Data tidak ada </option>';
				}
				$("select[name='inputDesa']").empty();
				$("select[name='inputDesa']").append(html);
			}
		});
}

function onSubmitKpm(){
	let id_kpm = $('#id_kpm').val();
	let nik_kpm = $('#nik_kpm').val();
	let nama_kpm = $('#nama_kpm').val();
	let alamat = $('#alamat').val();
	let cmb_status = $('#cmb_status').val();
	let cmb_status_dtks = $('#cmb_status_dtks').val();
	let inputDesa = $('#inputDesa').val();
	let layak = $('input[name="kelayakanRadio"]:checked').val();

	

	if(nik_kpm.length < 16){
		alert('nik harus 16 digit')
	}else if(nama_kpm == ''){
		alert('nama tidak boleh kosong');
	}else if(inputDesa == 0){
		alert('Desa harus dipilih')
	}else{
		var formData = new FormData($('#formKpm')[0]);
		formData.append('id_kpm',id_kpm);
		formData.append('kode_desa',inputDesa);
		formData.append('nama',nama_kpm);
		formData.append('nik',nik_kpm);
		formData.append('alamat',alamat);
		formData.append('layak',layak);
		formData.append('status',cmb_status);
		formData.append('dtks',cmb_status_dtks);

		$.ajax({
			url:acces_url.SaveKpm,
			type: 'POST',
			dataType: 'json',
			data: formData,
			contentType: false,
			processData: false,
			beforeSend:function(){
				$("body").css("cursor", "progress");
				$("#loading").removeAttr("style");
			},
			success:function(response) {
				$("body").css("cursor", "default");
			   if(response.state != true){
					alert(response.msg);
			   }else{
					alert('Pengaduan Berhasil diinput');
					document.getElementById("ListData").style.display = '';
					document.getElementById("formInput").style.display = 'none';
					searchdata();
			   }
			}
		})
	}
}

function EditKpm(idKpm){

	let postIdKpm = {'IdKpm':idKpm};

	$.ajax({
		url:acces_url.GetKpmById,
		type:'POST',
		data :postIdKpm,
		dataType:'json',
		beforeSend:function(){

		},
		success:function(response){
			let dtkelayakan = response.data.layak;
			let respKelayakan;
			if(dtkelayakan == 't'){
				respKelayakan = '1';
			}else{
				respKelayakan = '0';
			}
			let respStatus;
			if(response.data.status == 't'){
				respStatus = '1'
			}else{
				respStatus = '0'
			}

			let respStatusDtks;
			if(response.data.dtks == 't'){
				respStatusDtks = '1'
			}else{
				respStatusDtks = '0'
			}
			document.getElementById("ListData").style.display = 'none';
			document.getElementById("formInput").style.display = '';
			$('#id_kpm').val(response.data.id_kpm);
			$('#nik_kpm').val(response.data.nik);
			$('#nama_kpm').val(response.data.nama);
			$('#alamat').val(response.data.alamat);
			$("#cmb_status").val(respStatus).change();
			$("#cmb_status_dtks").val(respStatusDtks).change();
			GetInputKecamatan(response.data.kode_kec);
			getDataDesa(response.data.kode_kec,response.data.kode_desa);

			$('#radioLayak').find(':radio[name=kelayakanRadio][value="'+respKelayakan+'"]').prop('checked', true);
		}

	})
}

function HapusKpm(idKpm){
	let postIdKpm = {'IdKpm':idKpm};
	$.ajax({
		url:acces_url.GetKpmDataDelete,
		type:'POST',
		data :postIdKpm,
		dataType:'json',
		beforeSend:function(){

		},
		success:function(response){
			if(response.state){
				$('#modal-delete').modal({ backdrop: 'static' });
				$('.modal-title').empty().append('Konfirmasi');
				$('#text_confirm_delete').empty().append('Apakah Yakin Data Kpm Akan dihapus');
				$('#btn_confirm').attr('onclick', 'on_delete_confirm_kpm(' + idKpm + ')');

			}else{
				
				$('#modal-cant-delete').modal({ backdrop: 'static' });
				$('.modal-title').empty().append('Konfirmasi');
				$('#text_confirm').html('Data dengan no Kpm '+idKpm+ response.msg);
				$('#text_confirm_info').html();
			}
		}

	});
	//$('#modal-delete').modal('show');
}

function on_delete_confirm_kpm(idKpm){
	let postIdKpm = {'IdKpm':idKpm};
	$.ajax({
		url:acces_url.KpmDelete,
		type:'POST',
		data :postIdKpm,
		dataType:'json',
		beforeSend:function(){

		},
		success:function(response){
			if(response.state){
				$('#modal-delete').modal('hide');
				alert(response.msg);
			}else{
				$('#modal-delete').modal('hide');
				alert(response.msg);
			}
			searchdata();
		}

	});
}

function onCancel(){
	document.getElementById("ListData").style.display = '';
	document.getElementById("formInput").style.display = 'none';

}
 



console.log(SITE_URL);
let accessController = {
	GetRoles : SITE_URL+'User/getRoles',
	GetKecamatans : SITE_URL+'Wilayah/getKecamatan',
	GetDesas : SITE_URL+'Wilayah/getDesa',
	SaveUser : SITE_URL+'User/SaveUser',
}

var oTable;
$(document).ready(function(){
	
	oTable = $("#TabelManajemenUser").DataTable({
		"fixedHeader": false,
		"processing": true, // for show progress bar  
		"serverSide": true, // for process server side  
		"filter": false, // this is for disable filter (search box)  
		"ordering": true,// for disable multiple column at once  
		"pagingType": "full_numbers",
		"lengthMenu": [[5,10,25, 50, 100, 200, -1], [5,25, 50, 100, 200, 'All']],
		"paging": true,
		order: [1, 'desc'],
		scrollCollapse: true,
		"ajax": {
			"url": SITE_URL + "user/get",
			"type": "POST",
			"datatype": "json",
		},
	
		"columns": [		
			{ "data": "no", "name": "no", orderable: false },
			{ "data": "username", "name": "username", orderable: false },
			{ "data": "email", "name": "email", orderable: false },
			{ "data": "fullName", "name": "fullName", orderable: false },
			{ "data": "role_name", "name": "role_name", orderable: false },
		]
	   
	});

});

let AddUser = () => {
    $('#FormUser')[0].reset();
    $("#TitleFormUser").html("Tambah Pengguna")
    $('#ModalFormUser').modal("show");
    GetRoles(null);
    $("#BtnSaveUser").attr("onClick", "SaveUser('Tambah');");
    $("#DivPassword", "#FormUser").show();

    $("select[name='Roles']").removeAttr("disabled");
    $("input[name='Username']").removeAttr("readonly");
    $("input[name='IdUser']", "#FormUser").val(0);
}

let GetRoles = idRole => {

	$.ajax({
		url:accessController.GetRoles,
		type: "GET",
		dataType: "json",
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(idRole == v.id_role){
						html += `<option value='${v.id_role}' selected> ${v.role_name} </option>`;
					}else{
						html += `<option value='${v.id_role}' > ${v.role_name} </option>`;
					}

					
				});
				$("select[name='roles']").empty();
					$("select[name='roles']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})

}

$('#roles').on('change',function(){
	let idRoles = this.value;
	if(idRoles === '3'){ // petugaskecamatan
		document.getElementById("Divkecamatan").style.display = "";
		document.getElementById("Divdesa").style.display = "none";
		GetKecamatan();
	}else if(idRoles === '4'){
		document.getElementById("Divkecamatan").style.display = "";
		document.getElementById("Divdesa").style.display = "";
		GetKecamatan();
		$('#kecamatan').on('change',function(){
			GetDesas(this.value,'');
		})
	}else{
		document.getElementById("Divkecamatan").style.display = "none";
		document.getElementById("Divdesa").style.display = "none";
	}
});




let GetKecamatan = (kodeKec) => {
	$.ajax({
		url:accessController.GetKecamatans,
		type: "GET",
		dataType: "json",
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_kec}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_kec}' > ${v.name} </option>`;
					}

					
				});
				console.log(html);
				$("select[name='kecamatan']").empty();
					$("select[name='kecamatan']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}

let GetDesas = (kodeKec,kodeDesa) =>{
	$.ajax({
		url:accessController.GetDesas,
		type: "POST",
		dataType: "json",
		data: {"kode_kec":kodeKec},
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
			$("body").css("cursor", "default");
			$("#loading").css("display", "none");
			if(response.Data.length > 0){
				let html = '';
				html += '<option value=-1> Pilih Salah Satu </option>';
				$.each(response.Data,(i,v) => {
					if(kodeKec == v.kode_kec){
						html += `<option value='${v.kode_desa}' selected> ${v.name} </option>`;
					}else{
						html += `<option value='${v.kode_desa}' > ${v.name} </option>`;
					}
					
				});
				$("select[name='desa']").empty();
					$("select[name='desa']").append(html);
			}else{
				html += '<option value=-1> Data Tidak ada </option>';
			}
			
		}
	})
}

function SaveUser(str){
	let roles = $('#roles').val();
	let username = $('#username').val();
	let password = $('#password').val();
	let email = $('#email').val();
	let firstName = $('#firstName').val();
	let middleName = $('#middleName').val();
	let lastName = $('#lastName').val();
	let address = $('#address').val();
	let phoneNumber = $('#phoneNumber').val();
	let mobileNumber = $('#mobileNumber').val();
	let kecamatan = $('#kecamatan').val();
	let desa = $('#desa').val();



	let param = '';
	if(roles === '3'){
		param = {
				"username":username,
				"password":password,
				"email":email,
				"firstName":firstName,
				"middleName":middleName,
				"lastName":lastName,
				"address":address,
				"phoneNumber":phoneNumber,
				"mobileNumber":mobileNumber,
				"kodeWilayah":kecamatan,
				"roles"	: roles
			};
	}else if(roles === '4'){
		param = {
			"username":username,
			"password":password,
			"email":email,
			"firstName":firstName,
			"middleName":middleName,
			"lastName":lastName,
			"address":address,
			"phoneNumber":phoneNumber,
			"mobileNumber":mobileNumber,
			"kodeWilayah":desa,
			"roles"	: roles
		};
	}else{
		param = {
			"username":username,
			"password":password,
			"email":email,
			"firstName":firstName,
			"middleName":middleName,
			"lastName":lastName,
			"address":address,
			"phoneNumber":phoneNumber,
			"mobileNumber":mobileNumber,
			"kodeWilayah":0,
			"roles"	: roles
		};
	}

	$.ajax({
		url:accessController.SaveUser,
		type: "POST",
		dataType: "json",
		data: param,
		beforeSend:function(){
			$("body").css("cursor", "progress");
			$("#loading").removeAttr("style");
		},
		success:function(response) {
		   $("body").css("cursor", "default");
		   if(response.state == -1){
				alert(response.msg);
		   }else{
				alert(response.msg);
				$('#ModalFormUser').modal("hide");
				oTable.ajax.reload();
		   }
			
		}
	})
}


